package com.yedam.list;

import java.util.ArrayList;

import java.util.List;

public class ListExample {
	public static void main(String[] args) {
		
		//List<E>
		List<String> list = new ArrayList<String>();
		
		//list 데이터 추가
		list.add("Java"); // 인덱스가 0 인곳에 추가
		list.add("JDBC"); // 인데스가 1 인곳에 추가
		list.add("Servlet/JSP"); // 인덱스가 2 인곳에 추가
		// 인덱스가 2 인곳에 추가를 해주세요. -> JSP :인덱스 3으로 밀리고 
		// DataBase가 인덱스 2에 추가
		list.add(2, "DataBase"); //0-> 자바, 1->JDBC, 2-> DataBase, 3->JSP, 4-> iBATIS 
		list.add("iBATIS"); // 인덱스가 4 인곳에 추가
		
		//list 크기 , length == size
		int size = list.size();
		System.out.println("총 객체 수 : "+size);
		System.out.println();
		
		//list 객체 가져오기
		String skill =list.get(2);
		System.out.println("Index 2 : "+ skill);
		System.out.println();
		
		
		//list 크기만큼 반복문 돌리는 방법
		for(int i=0; i<list.size();i++) {
			String str = list.get(i);
			System.out.println(i+" : " +str);
		}
		System.out.println();
		
		//list 삭제
		list.remove(2);
		
		for(int i=0; i<list.size();i++) {
			String str = list.get(i);
			System.out.println(i+" : " +str);
		}
		System.out.println();
		list.remove("JDBC");
		for(int i=0; i<list.size();i++) {
			String str = list.get(i);
			System.out.println(i+" : " +str);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}

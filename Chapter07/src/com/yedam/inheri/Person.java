package com.yedam.inheri;

public class Person extends People { //People의 자식클래스

	public int age;
	
	//자식 객체를 만들때, 생성자를 통해서 만든다.
	//super() 를 통해서 부모 객체를 생성한다.
	//여기서 super() 의미하는 것은 부모의 생성자를 호출
	//따라서 자식 객체를 만들게되면 부모 객체도 같이 만들어진다.
	public Person(String name, String ssn) { //자식의 생성자
		super(name, ssn); // super <-부모의 매소드, 생성자 가져올때 씀. (부모생성자)
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

package com.yedam.inter5;

public class Application {

	public static void main(String[] args) {
		Vehicle v1 = new Bus();
		
//		drive(v1);
//		
//		Vehicle v2 = new Taxi();
//		driver(v2);
		
		v1.run();
//		v1.checkFare();
		
//		Bus bus1 = new Bus();
//		Taxi taxi = (Taxi)bus1;  // 자동타입변환 안됨;
		
		
		Bus bus = (Bus)v1;
		
		bus.run();
		bus.checkFare();
		
		drive(new Bus());
		drive(new Taxi());
	}
	public static void drive(Vehicle vehicle) {
		if (vehicle instanceof Bus) {
			Bus bus = (Bus) vehicle;
			bus.checkFare();
		}
		vehicle.run();
	}

}



package com.yedam.abs;

public class Cat extends Animal {

	public Cat() {
		this.kind = "포유류";
	}
	
	@Override
	public void sound() {
		//추상클래스 안에 추상 메소를 사용하게되면 오버라이딩 필수!!!
		System.out.println("야옹");
		
	}
	
}

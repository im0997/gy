package com.yedam.access;

public class Application {
	public static void main(String[] args) {
		Child child = new Child();
		
		child.lastName = "또치";
		child.age = 20;
		
//		System.out.println("내 이름은 : "+ child.firstName + child.lastName);
//		System.out.println("DNA : " + child.DNA);
		
		//Pㅁrent 클래스 -> bloodType을 private로 설정
		//Child 클래스 -> Parent클래스의 bloodType 사용 x
		child.showInfo();
		System.out.println("나이 : "+ child.age);
		System.out.println();
	}
	
}

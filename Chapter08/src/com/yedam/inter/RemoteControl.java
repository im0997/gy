package com.yedam.inter; //8(1) + (4)다중인터페이스

public interface RemoteControl extends Searchable{
	
	//상수 (static final) 생략가능
	public static final int MAX_VOLUME = 10;
	public int MIN_VOLUME = 0;
	
	//추상 메소드 (abstract) 생략가능
	public void turnOn();
	public abstract void turnOff();
	public void setVolume(int volume);

}

package com.yedma.study2;

public class RPGgame implements Keypad {

	//필드
	private int nowmode;
	
	
	//생성자
	public RPGgame() {
		nowmode = NORMAL_MODE;
		System.out.println("RPGgame 실행");
	}

	//메소드
	@Override
	public void leftUpButton() {
		System.out.println("캐릭터가 위쪽으로 이동한다");
	}

	@Override
	public void leftDownButton() {
		System.out.println("캐릭터가 아래쪽으로 이동한다");
	}
	
	@Override
	public void rightUpButton() {
		if(nowmode == Keypad.NORMAL_MODE) {
			System.out.println("캐릭터가 한칸단위로 점프한다.");
		} else if(nowmode == Keypad.HARD_MODE) {
			System.out.println("캐릭터가 두칸단위로 점프한다.");
		} 
	}
	@Override
	public void rightDownButton() {
		if(nowmode == Keypad.NORMAL_MODE) {
			System.out.println("캐릭터가 일반 공격.");
		} else if(nowmode == Keypad.HARD_MODE) {
			System.out.println("캐릭터가 HIT공격.");
		}
	}

	@Override
	public void changeMode() {
		if(nowmode == Keypad.NORMAL_MODE) {
			this.nowmode = Keypad.HARD_MODE;
			System.out.println("현재모드 : HARD_MODE");
		} else if(nowmode == Keypad.HARD_MODE) {
			this.nowmode = Keypad.NORMAL_MODE;
			System.out.println("현재모드 : NOTMAL_MODE");
		}

	}
	
	

	

}

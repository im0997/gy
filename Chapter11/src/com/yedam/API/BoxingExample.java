package com.yedam.API;

public class BoxingExample {
	public static void main(String[] args) {
		//Boxing 기본타입 -> 객체포장
		//Boxing  new Integer // Integer.valueOf
		Integer obj1 = new Integer(100);
		Integer obj2 = new Integer("200");
		Integer obj3 = new Integer("300");
		Integer obj4 = Integer.valueOf(100);
		Integer obj5 = Integer.valueOf("500");
		
		
		//unBoxing
		int value1 = obj1.intValue();
		int value2 = obj2.intValue();
		int value3 = obj3.intValue();
		int value4 = obj4.intValue();
		int value5 = obj5.intValue();
		
		System.out.println(value1);
		System.out.println(value2); // 문자열 > 객체>>int타입
		System.out.println(value3);
		System.out.println(value4);
		System.out.println(value5);
		
		//자동 박싱
		Integer obj6 = 600;
		System.out.println("vlaue : "+obj6.intValue());
		
		//대입 시 자동 언박싱
		
		int value6 = obj6;
		System.out.println("vlaue : "+value6);
		
		//연산 시 자동 언박싱
		int value7 = obj6+300;
		System.out.println("value : " + value7);
		
		//포장객체 값 비교
		
		Integer obj7 =100;
		Integer obj8 =100;
		System.out.println(obj7==obj8);
		System.out.println(obj7.intValue() == obj8.intValue());
		
		boolean obj9 = true;
		boolean obj10 =true;
		System.out.println("boolean 비교 : "+ (obj9==obj10));
		
		Byte obj11 = -100;
		Byte obj12 = -100;
		System.out.println("Byte 비교 : " + (obj11 == obj12));
		
		
		
	}
}

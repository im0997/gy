package com.yedam.test;

import java.util.Scanner;

public class test2 {
	public static void main(String[] args) {
//		[2번 문제]
//		삼각형의 3변의 길이 a, b, c가 입력으로 주어진다.
//		여기서 입력되는 변의 관계는 a ≤ b ≤ c 이다. 
//		그 삼각형이 무슨 삼각형인지 출력하시오.
//		※ 정삼각형, 이등변삼각형의 성질을 확인 후 해당 문제에 대입해서 풀어 본다.
		
		Scanner sc = new Scanner(System.in);
		
		int a = Integer.parseInt(sc.nextLine());
		int b = Integer.parseInt(sc.nextLine());
		int c = Integer.parseInt(sc.nextLine());
		
		if (a==b && b==c) {
			System.out.println("정삼각형");
		} else if(a==c || a==b ||b==c) {
			System.out.println("이등변삼각형");
		} else {
			System.out.println("일반 삼각형");
		}
		
	}
}

package com.yedam.poly;

public class KumhoTire extends Tire {

	public KumhoTire(String location, int maxRocation) {
		super(location, maxRocation);
	}
	
	@Override
	public boolean roll() { //수명 남음.
		++accRotation;
		if(accRotation < maxRotation) {
			System.out.println(location + "KumhoTire 수명 : "+ (maxRotation - accRotation) +"회");
			return true;
		} else { //수명 다함.
			System.out.println("###"+location +"KumhoTire 펑크"+ "###");
			return false;
		}
	}
}

package com.yedam.tset1129;

public interface Keypad { //	1) Keypad 인터페이스를 정의한다.

//- 다음과 같이 상수 필드를 정의한다.
	//필드
	
	public static final int NORMAL_MODE = 0;
	public static final int HARD_MODE = 1;
	
//- 다음과 같이 추상 메서드를 정의한다.
	
	//추상 메서드
	public abstract void leftUpButton(); 
	public abstract void leftDownButton();
	public abstract void rightUpButton();
	public abstract void rightDownButton();
	public abstract void changeMode();

}

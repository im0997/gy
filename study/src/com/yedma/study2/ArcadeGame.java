package com.yedma.study2;

public class ArcadeGame implements Keypad {
	
	//필드
	private int nowmode2;
	
	//생성자
	public ArcadeGame() {
		nowmode2 = NORMAL_MODE;
		System.out.println("ArcadeGame 실행");
	}

	//메소드
	@Override
	public void leftUpButton() {
		System.out.println("캐릭터가 앞쪽으로 이동한다");
	}

	@Override
	public void leftDownButton() {
		System.out.println("캐릭터가 뒤쪽으로 이동한다");
	}

	@Override
	public void rightUpButton() {
		if(nowmode2 == Keypad.NORMAL_MODE) {
			System.out.println("캐릭터가 일반 공격.");
		} else if(nowmode2 == Keypad.HARD_MODE) {
			System.out.println("캐릭터가 연속 공격.");
		}
		
	}

	@Override
	public void rightDownButton() {
		if(nowmode2 == Keypad.NORMAL_MODE) {
			System.out.println("캐릭터가 HIT공격.");
		} else if(nowmode2 == Keypad.HARD_MODE) {
			System.out.println("캐릭터가 Double HIT공격.");
		}
	}

	@Override
	public void changeMode() {
		if(nowmode2 == Keypad.NORMAL_MODE) {
			this.nowmode2 = Keypad.HARD_MODE;
			System.out.println("현재모드 : HARD_MODE");
		} else if(nowmode2 == Keypad.HARD_MODE) {
			this.nowmode2 = Keypad.NORMAL_MODE;
			System.out.println("현재모드 : NOTMAL_MODE");
		}

	}

	
}

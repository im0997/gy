package com.yedam.access;

import com.yedam.inheri.A;

public class B extends A {
	public B() {
		//부모생성자 호출 & 객체생성
		super();
		//부모 필드 접근
		this.field = "value";
		//부모 메소드 접근
		this.method();
	}

}

package com.yedam.test2;

public class EmpDept extends Employee { // 상속 시 부모의 객체도 상속, 객체생성> 생성자 
//	2) EmpDept 클래스를 정의한다.
//- Employee 클래스를 상속한다.
//- 추가로 부서이름을 필드로 가지며 생성자를 이용하여 값을 초기화한다.
//- 추가된 필드의 getter를 정의한다.
//- Employee 클래스의 메소드를 오버라이딩한다.
//(1) public void getInformation() : 이름과 연봉, 부서를 출력하는 기능
//(2) public void print() : "수퍼클래스\n서브클래스"란 문구를 출력하는 기능
	
	//필드
	//- 추가로 부서이름을 필드로 가지며 
	public String teamName;
	//생성자
	//- Employee 클래스를 상속한다.
	// 생성자를 이용하여 값을 초기화한다. // String teamName;
	public EmpDept(String name, String salary, String teamName) { // 부모클래스 생성자 객체
		super(name, salary);		//부모클래스의 생성자 호출, 객체생성
		this.teamName = teamName;
	}
	//메소드
	//- 추가된 필드의 getter를 정의한다.
	public String getTeamName() {
		return teamName;
	}

	//- Employee 클래스의 메소드를 오버라이딩한다.
	@Override
	//: 이름과 연봉, 부서를 출력하는 기능
	public void getInformation() {
		super.getInformation();
		System.out.println(" 부서 : " + teamName);
	}
	@Override
	//: "수퍼클래스\n서브클래스"란 문구를 출력하는 기능
	public void print() {
		super.print();
		System.out.println("서브클래스");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
	



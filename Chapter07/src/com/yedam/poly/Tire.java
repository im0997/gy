package com.yedam.poly;

public class Tire {
	//필드
	public int maxRotation; //최대 회전수(타이어 수명)
	public int accRotation; //누적 회전수(현재까지의 회전 수)
	public String location; //타이어 위치
	
	//생성자
	public Tire(String location, int maxRocation) {
		this.location = location;
		this.maxRotation = maxRotation;
	}
	
	//메소드
	
	//타이어가 굴러가는 메소드
	public boolean roll() { //수명 남음.
		++accRotation;
		if(accRotation < maxRotation) {
			System.out.println(location + "Tire 수명 : "+ (maxRotation - accRotation) +"회");
			return true;
		} else { //수명 다함.
			System.out.println("###"+location +"Tire 펑크"+ "###");
			return false;
		}
	}
	
}

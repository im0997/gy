package com.yedam.API;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Example22 {
	public static void main(String[] args) {
		//1. 문자열 뒤집기 -반복문 조건만 잘 주면 됨.
		
//		String str ="1234567989";
//		System.out.println(str);
//		
//		byte[] byte1 = str.getBytes(); 
//		String str1 = new String(byte1);
//		for(int i=9; i>=0; i--) {
//			String str2 = new String(byte1,i,1);
//			System.out.print(str2);
//		}
//		System.out.println("\n"+"===============");
//		for(int i=str.length()-1; i>=0; i--) {
//			System.out.print(str.charAt(i));
//		}
//		System.out.println("\n" +"===============");
		
		
		
	
		//2. 세개의 임의 단어 3개 중 가장 짧은 단어와 길이 출력
//		String str2 = "김김또치";
//		String str3 = "김또치";
//		String str4 = "또치";
//		byte[] byte2 = str2.getBytes();
//		byte[] byte3 = str3.getBytes();
//		byte[] byte4 = str4.getBytes();
//		
//		if(byte2.length <= byte3.length && byte2.length <= byte4.length) {
//			String str5 = new String(byte2);
//			System.out.println(str5 + byte2.length);
//		} else if (byte2.length <= byte3.length && byte2.length >= byte4.length && byte4.length <= byte3.length) {
//			String str7 = new String(byte4);
//			System.out.println(str7 + byte4.length);
//		} else if(byte2.length >= byte3.length && byte3.length <= byte4.length ) {
//			String str6 = new String(byte3);
//			System.out.println(str6 + byte3.length);
//		}
//
//		System.out.println("===============");
//		String str5 =str2 + " : " + str2.length();
//		if(str2.length() > str3.length()) {
//			str5=str3+" : "+str3.length();
//			// 3> 2
//			if(str3.length() > str4.length()){
//				str5=str4 +" : "+str4.length();
//			}
//		} else {
//			if(str2.length() > str4.length()) {
//				str5=str4 +" : "+str4.length();
//			}
//		}
//		System.out.println(str5);
//		System.out.println("===============");
		
		//3. ID와 비밀번호를 입력받아 ID가 3글자보다 크고 비밀번호가 8글자이상
		//   사용할 수 있는 ID와 PW입니다. 출력하기
		// 해당 아이디에 숫자 몇개, 문자 몇개, 특수문자 몇개.
		Scanner sc =new Scanner(System.in);
//		System.out.println("입력>");
//		String id = sc.nextLine();
//		String pw = sc.nextLine();
//
//		System.out.println("ID입력수"+id.length());
//		System.out.println("PW입력수"+pw.length());
//		if (id.length() > 3 && pw.length() >=8) {
//				System.out.println("사용할수있는 ID와 PW입니다.");
//		} else {
//			System.out.println("사용할수 없는 ID와 PW입니다.");
//		}
		
		
		
		//4. 생년월일 입력 후 나이 출력하기 (220101-> 2022년생,230202->1923년생)
		//예시) 입력 : 950101 
		//     출력 : 28
		//	   입력 : 001013
		//     출력 : 23
		
		//1. 계신삭을 세워야한다.
		//11.1) 00년 이후 22년 이전에 출생한 사람 나이 구하기 (22-00)+1
		//11.2) 00년 이전 23년 이후에 출생한 사람 나이 구하기 (122-99)+1
		//99->24( x - 99 = 24), x=99+24 , (122-생년원일)+1
		//98->25
		
		System.out.println("생년월일 입력?>");
		String a1 = sc.nextLine();
		int c1 = Integer.parseInt(a1.substring(0,2));
		if (c1<=22) {
			System.out.println("나이 : " + (22-c1+1));
		} else if (c1>22) {
			System.out.println("나이 : " + (122-c1+1));
		}
				
}
}

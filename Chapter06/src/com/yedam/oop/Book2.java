package com.yedam.oop;

public class Book2 {
	/*
	 * 책 이름 : 혼자 공부하는 자바
	 * # 내용 
	 * 1) 종류 : 학습서
	 * 2) 가격 : 24000원
	 * 3) 출판사 : 한빛미디어
	 * 4) 도서번호 : yedam-001
	 * 
	 * 책 이름 : 이것이 리눅스다
	 * # 내용
	 * 1) 종류 : 학습서
	 * 2) 가격 : 32000원
	 * 3) 출판사 : 한빛미디어
	 * 4) 도서번호 : yedam-002
	 * 
	 * 책 이름 : 자바스크립트 파워북
	 * # 내용
	 * 1) 종류 : 학습서
	 * 2) 가격 : 22000원
	 * 3) 출판사 : 어포스트
	 * 4) 도서번호 : yedam-003
	 * 
	 *  위 내용을 출력할 수 있도록 Book 클래스를 생성하고 내용을 출력 할수 있도록
	 *  getInfo() 메소드를 추가하세요. 
	 *  
	 *  메인 클래스에서 각각의 객체를 생성하고 getInfo()로 객체의 속성을 출력하시오.
	 */
	
	//필드
	
	String bookName;
	String kind = "학습서";
	int price;
	String publisher;
	String isbn;
	
	//생성자
	public Book(String bookName, int price, String publisher, String isbn) {
	this.bookName =bookName;
	this.price = price;
	this.publisher = publisher;
	this.isbn =isbn;
	}

	// 메소드
	
	void gerInfo() {
		System.out.println(bookName);
		System.out.println(price);
		System.out.println(publisher);
		System.out.println(isbn);
	}
}

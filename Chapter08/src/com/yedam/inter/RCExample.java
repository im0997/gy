package com.yedam.inter; //8(1) + (4)다중인터페이스

public class RCExample {
	public static void main(String[] args) {
		RemoteControl rc;
		
		rc = new SmratTV(); // 자식클래스로 객체화 하면서 메소드 실행.
		//SmartTV클래스 - >implements RemoteControl( + Searchable)
		//RemoteControl( + Searchable) -> Searchable을 상속받고 있기 때문에
		
		//RemoteControl
		rc.turnOn();
		rc.setVolume(40); // 자식클래스에 재정의해놓은 메소드 실행
		rc.turnOff();
		//Searchable
		rc.search("www.google.com");
		
		
		
//		Searchable sc = new SmratTV();
//		sc.search("www.google.com");
		
//		rc = new Audio();
//		
//		rc.turnOn();
//		rc.setVolume(1);
//		rc.turnOff();
//		
//		
//		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}

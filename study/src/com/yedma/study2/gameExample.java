package com.yedma.study2;

public class gameExample {
	public static void main(String[] args) {
		Keypad kp;
		
		kp = new RPGgame();
//		RPGgame 실행
//		캐릭터가 위쪽으로 이동한다.
//		캐릭터가 한칸단위로 점프한다.
//		현재 모드 : HARD_MODE
//		캐릭터가 두칸단위로 점프한다.
//		캐릭터가 HIT 공격.
//		캐릭터가 아래쪽으로 이동한다.
//		현재 모드 : NORMAL_MODE
//		캐릭터가 일반 공격.
		
		kp.leftUpButton();
		kp.rightUpButton();
		kp.changeMode();
		kp.rightUpButton();
		kp.rightDownButton();
		kp.leftDownButton();
		kp.changeMode();
		kp.rightDownButton();
		
		System.out.println("==================");
		
//		ArcadeGame 실행
//		캐릭터가 앞쪽으로 이동한다.
//		캐릭터가 일반 공격
//		캐릭터가 뒤쪽으로 이동한다.
//		현재 모드 : HARD_MODE
//		캐릭터가 연속 공격.
//		캐릭터가 앞쪽으로 이동한다.
//		캐릭터가 Double HIT 공격.
		
		kp = new ArcadeGame();
		kp.leftUpButton();
		kp.rightUpButton();
		kp.leftDownButton();
		kp.changeMode();
		kp.rightUpButton();
		kp.leftUpButton();
		kp.rightDownButton();
		
		
		
				
	}
}

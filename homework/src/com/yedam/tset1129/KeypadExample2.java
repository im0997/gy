package com.yedam.tset1129;

import java.util.Scanner;

public class KeypadExample2 {
		public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);
		Keypad kp;
		int a = (int) ((Math.random()*2) +1);
		System.out.println(a);
		if (a ==1) {
			kp = new RPGgame();
		} else {
			kp = new ArcadeGame();
		}
		
		while(true) {
			System.out.println("============================================================================================");
			System.out.println("<< 1.LeftUP | 2.LeftDown | 3.RightUp | 4.RightDown | 5.ModeChange | 0.GameChange | 9.EXIT >>");
			System.out.println("============================================================================================");
			System.out.print("choice>>");
			int selecno = Integer.parseInt(sc.nextLine());
			
			if (selecno ==1) {
				kp.leftUpButton();
			} 
			else if(selecno ==2) {
				kp.leftDownButton();
			} 
			else if(selecno ==3) {
				kp.rightUpButton();
			} 
			else if(selecno ==4) {
				kp.rightDownButton();
			} 
			else if(selecno ==5) {
				kp.changeMode();
			} 
			else if(selecno ==0) {
				if (kp == new RPGgame()) {
					kp = new ArcadeGame();
				} else{
					kp = new RPGgame();
				}
			} 
			else if(selecno ==9) {
				System.out.println("EXIT");
				break;
			} 
		}
		
		
		
	}


}

package com.yedam.map;

import java.util.*;

public class HashTableExample {
	//아이디와 비밀번호 검사하는 프로그램
	public static void main(String[] args) {
		Map<String, String> map = new Hashtable<>();
		Scanner sc = new Scanner(System.in);
		//(Key,Value) 키는 중복안됨, V은 중복 가능
		map.put("spring", "12"); 
		map.put("summer", "123");
		map.put("fall", "1234");
		map.put("winter", "12345");
		
		//map이 가지고 있는 키를 확인 => 아이디가 존재하는지 확인
		while(true) {
			System.out.println("아이디와 비밀번호 입력>");
			System.out.println("아이디>");
			String id = sc.nextLine();
			System.out.println("비밀번호>");
			String pw = sc.nextLine();
			
			if(map.containsKey(id)) {
				//key를 활용해서 value를 가지고 온 다음 pw 비교
				if(map.get(id).equals(pw)) {
					System.out.println("로그인되었습니다.");
					break;
				} else {
					System.out.println("비밀번호가 일치하지 않습니다.");
				}
			} else {
				System.out.println("입력하신 아이디가 존재하지 않습니다.");
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}

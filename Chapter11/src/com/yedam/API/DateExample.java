package com.yedam.API;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.event.SwingPropertyChangeSupport;

public class DateExample {
	public static void main(String[] args) {
		//날짜를 가져오기 위해 Date 클래스 호출
		Date now = new Date();
		System.out.println(now.toString());
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일 hh시 mm분 ss초");
		
		
		System.out.println(sdf.format(now));
		
		// Calendar 클래스
		
		Calendar now2 = Calendar.getInstance();
		int year = now2.get(Calendar.YEAR);
		int month = now2.get(Calendar.MONTH)+1;
		int day = now2.get(Calendar.DAY_OF_MONTH);
		int week =now2.get(Calendar.DAY_OF_WEEK);
		
		String strWeek = "";
		
		switch (week) {
		case Calendar.MONDAY:
			strWeek ="월";
			break;
		case Calendar.TUESDAY:
			strWeek ="화";
			break;
		case Calendar.WEDNESDAY:
			strWeek ="수";
			break;
		case Calendar.THURSDAY:
			strWeek ="목";
			break;
		case Calendar.FRIDAY:
			strWeek ="금";
			break;
		case Calendar.SATURDAY:
			strWeek ="토";
			break;
		case Calendar.SUNDAY:
			strWeek ="일";
			break;
		}
		int amPm = now2.get(Calendar.AM_PM);
		String strAmpm = "";
		if(amPm == Calendar.AM) {
			strAmpm ="오전";
		} else {
			strAmpm = "오후";
		}
	
		int hour = now2.get(Calendar.HOUR);
		int minute = now2.get(Calendar.MINUTE);
		int second = now2.get(Calendar.SECOND);
		
		System.out.println(year+"년");
		System.out.println(month+"월");
		System.out.println(day+"일");
		System.out.println(strWeek+"요일");
		System.out.println(strAmpm+" ");
		System.out.println(hour+"시");
		System.out.println(minute+"분");
		System.out.println(second+"초");
	}
}

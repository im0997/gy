package com.yedam.inheri;

public class OverrideExam {
	public static void main(String[] args) {
		Child child = new Child();
		
		child.method1();	//자식의 메소드 재정의 -> 호출
		child.method2();	//부모메소드 호출
		child.method3();	//자식메소드 호출
	}
}

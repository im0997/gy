package com.yedam.inter;

public class LGWashingMachine implements WashingMachine{


	@Override
	public void startBtn() {
		System.out.println("세탁을 시작합니다.");
	}

	@Override
	public void pauseBtn() {
		System.out.println("일시정지 합니다.");
		
	}

	@Override
	public void stopBtn() {
		System.out.println("세탁을 멈춥니다."); 
		
	}

	@Override
	public int changeSpeed(int speed) {
		int nowSpeed =0;
		switch (speed) {
		case 1:
			nowSpeed =20;
			break;
		case 2:
			nowSpeed =50;
			break;
		case 3:
			nowSpeed =100;
			break;

		default:
			break;
		}
		return nowSpeed;
	}
	
	@Override
	public void dry() {
		System.out.println("건조 코스 진행");
		
	}

}

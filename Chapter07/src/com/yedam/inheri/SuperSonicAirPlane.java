package com.yedam.inheri;

public class SuperSonicAirPlane extends AirPlane{
	//필드
	//일반비행
	public static final int NORMAR = 1;
	//초음속비행
	public static final int SUPERSONIC =2;
	
	public int flyMode = NORMAR;

	
	
	//생성자
	
	//메소드
	@Override
	public void fly() {
		if(flyMode == SUPERSONIC) {
			System.out.println("초음속 비행 모드");
		} else {
			super.fly(); //부모메소드 실행
		}
		
	}
	
	
	

}

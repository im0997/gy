package com.yedam.queue;

import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {
	public static void main(String[] args) {
		
		
		Queue<Message> messageQueue =new LinkedList<>();
		
		messageQueue.offer(new Message("sendMail", "홍길동"));
		messageQueue.offer(new Message("sendMS", "김또치"));
		messageQueue.offer(new Message("sendKaKaoTalk", "마이콜"));
		
		while(!messageQueue.isEmpty()) {
			Message message =messageQueue.poll();
			
			switch (message.command) {
			case "sendMail":
				System.out.println(message.to + "님에게 메일을 보냅니다.");
				break;
			case "sendMS":
				System.out.println(message.to + "님에게 문자를 보냅니다.");
				break;
			case "sendKaKaoTalk":
				System.out.println(message.to + "님에게 카톡을 보냅니다.");
				break;
			}
		}
		
		
		
		
		
		
		
		
		
	}
}

package com.yedam.oop;

public class Student {
	   // 학생의 이름 : 고길동
	   // 학생의 학교 : 예담고등학교
	   // 학생의 학번 : 221124

	   // 학생의 이름 : 김둘리
	   // 학생의 학교 : 예담고등학교
	   // 학생의 학번 : 221125

	   // 학생의 이름 : 김또치
	   // 학생의 학교 : 예담고등학교
	   // 학생의 학번 : 221126

	   // 객체 생성시, 두 가지 경우로 필드를 초기화 한다.
	   // 1) 생성자를 통한 필드 초기화
	   // 2) 객체 필드에 접근하여 필드 초기화
	   // 각 필드를 초기화 한 후, getInfo() 메소드를 통하여 위 내용을 출력하시오
		
	//필드
	String name;
	String school;
	int number;
	int kor;
	int math;
	int eng;
	
	//생성자 
	public Student(String name, String school, int number) {
		this.name = name;
		this.school = school;
		this.number = number;		
	}
	public Student () {
	}
	//메소드
	void getInfo() {
		System.out.println("학생의 이름은 : " + name);
		System.out.println("학생의 학교 : " + school);
		System.out.println("학생의 학번 : " + number);
	}
	int sum() {
		return kor+eng+math;
	}
	double avg() {
		double avgs = sum()/(double)3;
		return avgs;
	}
}

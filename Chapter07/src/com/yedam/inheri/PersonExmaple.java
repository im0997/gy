package com.yedam.inheri;

public class PersonExmaple {
	public static void main(String[] args) {
		Person p1 = new Person("고희동", "123456-9876541");
		
		//부모클래스의 필드 호출
		//Person() 실행할 때, 부모객체를 만들어서
		//그 안에 있는 인스터스 필드를 가져와서 실행.
		System.out.println("name : " + p1.name);
		System.out.println("ssn : "+p1.ssn);
		
		//자식 클래스의 필드 호출
		//Person() 실행할 때, 자신이 가지고 있는 인스터스 필드
		//호출하여 실행
		System.out.println("age : " + p1.age);
		
	}
}

package com.yedam.inheri;

public class Parent {
	//부모클래스
	// 1) 상속할 필드 정의
	protected String firstName = "Lee";
	protected String LastName;
	protected String DNA = "B";
	int age; //default <다른패키지에서 사용 못함.
	
	// 2) 상속 대상에서 제외
	private char bloodType = 'B';
	
	//메소드
	//오버라이딩 예제
	protected void method1() {
		System.out.println("parent class -> method1 Call");
	}
	public void method2() {
		System.out.println("parent class -> method2 Call");
	}
		
	
	
	
}

package com.yedam.study;

import java.util.Scanner;

public class homework1125 {
	public static void main(String[] args) {
		// 문제2) 다음은 키보드로부터 상품 수와 상품 가격을 입력받아서
				// 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총 합을 구하는 프로그램을 작성하세요.
				// 최고값 > Max값찾기> 해당제품 제외한 제품들의 합 = 모든제품 함 - 최고가격
				// 1.최고가격제품 찾을때 인덱스 또는 값을 따로 저장. 
				// 2. 반복문 한번 더 돌려서 해당 제품 제외 합을 한다.
		
				// 1) 메뉴는 다음과 같이 구성하세요.
				// 1.상품 수 | 2.상품 및 가격입력 | 3.제품별 가격 | 4.분석 | 5.종료
				// while(){ 메뉴출력
				//          메뉴 진입할 수 있는 조건문 통해 구별}
				// 2) 입력한 상품 수만큼 상품명과 해당 가격을 입력받을 수 있도록 구현하세요.
				// 	  입력한상품의수 = 배열 -> 입력시 마다 배열이 달라진다. (스캐너로 배열크기 할당.)
			 	// 	  상품클래스 > 필드 =상품명, 가격 설정.
				//    입력 -> 반복문 활둉(배열의 크기만큼 반복) 
				// 3) 제품별 가격을 출력하세요.
				//	  출력예시, "상품명 : 가격"
				//	  출력 -> 반복문활용(배열의크기만큼) -> 각방에있는 객체를 하나씩 꺼내옴.
				//	  객체가 가지고있는 필드(정보를 담은 변수0 -> 출력예시에 맞게 만든다.
				// 4) 분석기능은 최고 가격을 가지는 제품과 해당 제품을 제외한 제품들의 총합을 구합니다.
				// 
				// 5) 종료 시에는 프로그램을 종료한다고 메세지를 출력하도록 구현하세요.
				//	  반복문 종료. 종료직전 출력문 시행.
		
		//상품 수와 상품명을 입력받을 수 잇도록 Scanner 등록
		
		Scanner sc = new Scanner(System.in);
		
		Product[] pd = null;
		int productCount = 0;
		
		while(true) {
		System.out.println("1. 상품 수 | 2. 상품 및 가격 입력 | 3. 제품별 가격 | 4. 분석 | 5. 종료");
		System.out.println("입력>");
		
		String selecNo = sc.nextLine();
		// 1. 상품수 메뉴 만들기
		if( selecNo.equals("1")) {
			System.out.println("상품 수 입력 >");
			productCount = Integer.parseInt(sc.nextLine());						
		} else if (selecNo.equals("2")) {
			// 상품 수 입력받은 내용을 토대로 배열 크기 확정
			pd = new Product[productCount];
			//productCount =5
			//pd.length = 5
			for(int i=0; i<pd.length; i++) {
				// 상품 객체 생성
				// 반복문 실행시 마다 새로운 상품 만들기 위해서//새로운 공간에 객체생성 i 수만큼 (상품 수만큼?)
				Product product = new Product(); 
				System.out.println((i+1)+"번째 상품");
				
				System.out.println("상품명 >");
				//변수에 데이터 입력받고 객체에 데이터 넣는 방법
				
				String name= (sc.nextLine());
				product.ProductName = name;
				
				System.out.println("가격");
				// 데이터 입력 받음과 동시에 객체에 데이터 넣는방법
				product.price = Integer.parseInt(sc.nextLine());
				
				pd[i] = product;
				System.out.println("===========");
				
			}
			
		} else if (selecNo.equals("3")) {
			// 배열의 크기만 반복문 진행할때 배열의 각 인덱스(방번호)를 활용하여 객체(상품) 꺼내와서 객체(상품)에 정보를 하나씩 꺼내옴.
			for(int i=0; i<pd.length;i++) {
				String name = pd[i].ProductName;
				//String name = product.ProductName;
				int price = pd[i].price;
				//int price = pd[i].price;
				
				
				System.out.println("상품 명 : " + name);
				System.out.println("가격 명 : " + price);
				//System.out.println("가격 명 : " + pd[i].price);
				
			}
			
		} else if (selecNo.equals("4")) {
			// 1.최고가격제품 찾을때 인덱스 또는 값을 따로 저장. 
			// 2. 반복문 한번 더 돌려서 해당 제품 제외 합을 한다.
			int sum = 0;
			int max = pd[0].price;
			for (int i=0;i<pd.length; i++) {
				//제일 비싼 상품 가격 구하기
				if(max < pd[i].price) {
					max = pd[i].price;
				}
				//상품들의 가격 합계 구하기
				sum += pd[i].price;
			}
			System.out.println("제일 비싼 상품 가격 : " + max);
			System.out.println("제일 비싼 상품을 제외한 상품 총 가격 합 : " + (sum-max));
			
		} else if (selecNo.equals("5")) {
			System.out.println("프로그램 종료");
			break;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		}
	}
}

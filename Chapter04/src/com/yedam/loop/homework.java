package com.yedam.loop;

import java.util.Scanner;

public class homework {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		// 문제1) 차례대로 x와 y의 값이 주어졌을 때 어느 사분면에 해당되는지 출력하도록 구현하세요.
		// 각 사분면에 해당 하는 x와 y의 값은 아래를 참조하세요.
		// 제1사분면 : x>0, y>0
		// 제2사분면 : x<0, y>0
		// 제3사분면 : x<0, y<0
		// 제4사분면 : x>0, y<0
		// 문제출처, 백준(https://www.acmicpc.net/) 14681번 문제
		
		System.out.println("x 입력>");
		int x1 = Integer.parseInt(sc.nextLine());
		System.out.println("y 입력>");
		int y1 = Integer.parseInt(sc.nextLine());
		
		if(x1>0 && y1>0) {
			System.out.println("제1사분면에 해당됩니다.");
		} else if(x1<0 && y1>0) {
			System.out.println("제2사분면에 해당됩니다.");
		} else if(x1<0 && y1<0) {
			System.out.println("제3사분면에 해당됩니다.");
		} else if(x1>0 && y1<0) {
			System.out.println("제4사분면에 해당됩니다.");
		} 
		
		// 문제2) 연도가 주어졌을 때 해당 년도가 윤년인지를 확인해서 출력하도록 하세요.
		// 윤년은 연도가 4의 배수이면서 100의 배수가 아닐 때 또는 400의 배수일때입니다.
		// 예를 들어, 2012년은 4의 배수이면서 100의 배수가 아니라서 윤년이며,
		// 1900년은 100의 배수이고 400의 배수는 아니기 때문에 윤년이 아닙니다.
		// HiNT : 이중 IF문 사용
		// 문제출처, 백준(https://www.acmicpc.net/) 2753번 문제
		
		// 4의 배수 // 100의 배수 // 400의 배수
		System.out.println("연도 입력>");
		int year = Integer.parseInt(sc.nextLine());	
		if (year % 4 == 0 && year %100 != 0 ) {
			if( year % 4 == 0 && year % 400 == 0) {
				
			}
			System.out.println(year + "는 윤년입니다.");
		} else {
				 System.out.println(year + "년은 윤년이 아닙니다.");
		}
			
		// 문제3) 반복문을 활용하여 up & down 게임을 작성하시오.
		// 기회는 5번 이내로 맞추도록 하며, 맞추게 될 시에는 정답 공개 및 축하합니다.
		// 메세지 출력.
		// 사용자가 답안 제출 시, up, down이라는 메세지를 출력하면서 
		// 정답 유추를 할 수 있도록 한다.
		
		boolean flag = true;
		int chance;
		System.out.println("====insert Coin====");
		chance = Integer.parseInt(sc.nextLine());
		while(chance/5>0)	{

		// 한판에 500원
		System.out.println((chance/5>0) + "번의 기회가 있습니다.");
		System.out.println("1. up&down게임 | 2. 종료");
		System.out.println("입력 >"); 
		int gameNo = Integer.parseInt(sc.nextLine());
		
		//up&down
		if(gameNo == 1) {
			System.out.println("숫자를  하나를 입력하세요.");
			System.out.println("입력> ");	
			// 사용자
			String UD = sc.nextLine();
			//컴퓨터
			int randomNo = (int)(Math.random() * 5) + 1; // 1 ~ 10 사이의 정수
			
			if(UD.equals("up")) {
				if( UD == randomNo) {
					System.out.println("정답입니다." + randomNo + "축하합니다.");
					break;
				} else if(randomNo != randomNo) {
					System.out.println("틀렸습니다.");
				} else {
					System.out.println("잘못된 입력입니다.");
				}
			} else if (UD.equals("down")) {
				if(randomNo == randomNo) {
					System.out.println("정답입니다." + randomNo + "축하합니다.");
				}else if (randomNo != randomNo) {
					System.out.println("틀렸습니다.");
				}else {
					System.out.println("잘못된 입력입니다.");
				}
			} 
		// 종료
		else if (gameNo == 2) {
			System.out.println("종료.");		
			break;
		}
		
		
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		// 문제4) 차례대로 m과 n을 입력받아 m단을 n번째까지 출력하도록 하세요.
		// 예를 들어 2와 3을 입력받았을 경우 아래처럼 출력합니다.
		// 2 X 1 = 2
		// 2 X 2 = 4
		// 2 X 3 = 6
		
		System.out.println("m 입력 >");
		int m = Integer.parseInt(sc.nextLine());
		System.out.println("n 입력 >");
		int n = Integer.parseInt(sc.nextLine());
			
			for (int i=1; i<10; i++) {
				System.out.println(m +" = "+ (i*m));
			} for (int i=n; i<=n; i++)}
		
			// 구구단 출력
			// 만약, 2단 출력
			// 2*1, 2*2, 2*3, 2*4 ...,2*9
			// 2*i= 2
			
			for(int i=1; i<10; i++) {
				System.out.println("2 X " + i + " = " + (2*i));
			}
				
			// for문 안에 for문
			// 초기화식에 들어가는 변수를 두개를 고려.
			// 구구단 출력 (2단~9단)
			// 
			 for (int i=2; i<=9; i++) {
				 // i = 2 일때
				 // 아래반복문은 9번 돌아감
				 for(int j=1; j<=9; j++) {
					 System.out.println(i+ " X " + j + " = " + (i*j));
				 }
			 }
			 
			
			
			
			
			
			
			
			
			
			
			
			
			
//		
//		
		}	
}
	}
}


package com.yedam.oop;

import java.util.Scanner;
import com.yedam.access.Access;

public class Application20 {
	
	Access ac = new Access();
	Scanner sc = new Scanner(System.in);
	
	int speed;
	
	void run() {
		System.out.println(speed+ "으로 달립니다.");
		
	}
	//메소드 영억에 등록된 친구들.
	public static void main(String[] args) {
//		int speed2=speed; // static 영억 
//		run();
		Application20 app = new Application20(); //객체생성
		app.speed =5;
		app.run();
		
		
		Car myCar = new Car("포르쉐");
		Car yourCar = new Car("벤츠");
		
		myCar.run();
		yourCar.run();
		
		//정적 필드, 메소드를 부르는 방법
		//정적 멤버가 속해 있는 클래스명.필드 또는 메소드 명
		//1) 정적 필드 가져오는 방벙
		double piRatio= Calculator.pi;
		System.out.println(piRatio);
		//2) 정적 메소드 가져오는 방법
		int result = Calculator.puls(5, 6);
		System.out.println(result);
		
		//1) 모든 클래스에서 가져와서 사용할 수 있다 -> 공유의 개념
		//2) 너무 남용시 메모니 누수(부족) 현상이 발생할 수 있다.
		//3) **주의사항
		//   메소드 영역에 저장이 됩니다.
		//   static -> 아무대서나 사용 가능
		//  -정적 메소드에서 외부에 정의한 필드를 사용하려고 하면,
		//	static이 붙은 필드 또는 메소드만 사용 가능.
		//	static 붙이지 않고 사용하고 싶다면,
		//	해당 필드와 세모스다 속새있는 클래스를 인스턴스화 하여서
		//	인스턴스 필드와 메소드를 dot(.) 연산자를 통해 가져와서 사용.
		
		Person p1 = new Person("123123-123456" , "김또치");
		//final -> nation, ssn
		System.out.println(p1.nation);
		System.out.println(p1.ssn);
		System.out.println(p1.name);
		
		
//		p1.nation ="USA"; //final 필드
		
		
		//원 넓이
		System.out.println(5*5* ConstantNo.PI);
		
		
	}

}

package com.yedam.API;

import java.util.HashMap;

public class Example {
	public static void main(String[] args) {
		Object obj1 =new Object();
		Object obj2 =new Object();
		
		System.out.println(obj1.toString());
		System.out.println(obj2.toString());
		
		boolean result = obj1.equals(obj2);
		System.out.println(result);
		
		result = (obj1 == obj2);
		System.out.println(result);
		
		
		//재정의한 eqauls를 활용해서 객체 비교
		Member obj3 = new Member("blue");
		Member obj4 = new Member("blue");
		Member obj5 = new Member("red");
		
		if (obj3.equals(obj4)) { //equals 데이터를 비교하게끔 오버라이딩해놓음.
			System.out.println("obj3과 obj4는 동일 합니다.");
		} else {
			System.out.println("obj3과 4는 다릅니다.");
		}
		
		if (obj3.equals(obj5)) {
			System.out.println("obj3과 obj5는 동일 합니다.");
		} else {
			System.out.println("obj3과 obj5는 다릅니다.");
		}
		
		HashMap<Key,String> hashMap = new HashMap<>();
		//new Key(1) -> (100번지 ->)number-> 1(hashcode: 객체의 고유한 숫자(주소값))
		hashMap.put(new Key(1), "홍길동"); // 1로 데잍터를 hashmap 보관 -> 홍길동
		//new Key(1) -> 200번지? -> 3    //new 쓰면 번지가 달라짐.
		String value = hashMap.get(new Key(1)); // 1-> hashmap -> 홍길동
		
		System.out.println(value);
		
	}
}

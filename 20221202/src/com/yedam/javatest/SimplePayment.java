package com.yedam.javatest;

public class SimplePayment implements Payment {
	
	//필드
	double simp;
	
	//생성자
	public SimplePayment(double simp) {
		this.simp =simp;
		System.out.println("**간편결제시 할인정보");
	}
	
	@Override
	public void showInfo() {
		System.out.println("온라인 결제시 총 할인율 : "+(ONLINE_PAYMENT_RATIO + this.simp) );
		System.out.println("오프라인 결제시 총 할인율 : "+(OFFLINE_PAYMENT_RATIO + this.simp) );
	}
	@Override
	public int online(int price) {
		return (int) (price-(ONLINE_PAYMENT_RATIO + this.simp)*price);
	}
	@Override
	public int offline(int price) {
		return (int)(price-(OFFLINE_PAYMENT_RATIO + this.simp)*price);
	}
	

}
